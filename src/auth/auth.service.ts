import { Injectable } from '@nestjs/common';
import { UsersService } from 'src/users/users.service';
import { LoginDto } from './dtos/login.dto';
import { SignupDto } from './dtos/signup.dto';
import { ICredentialsType } from './types/credentials.type';
import * as bcrypt from 'bcrypt';

@Injectable()
export class AuthService {
  constructor(private readonly userService: UsersService) {}
  async login(context: LoginDto): Promise<ICredentialsType | undefined> {
    const user = await this.userService.findByEmail(context.email);
    if (!user) return;
    const isMatchPassword = await bcrypt.compare(
      context.password,
      user.password,
    );
    if (!isMatchPassword) return;

    return { _id: user._id, username: user.username, email: user.email };
  }

  async signup(context: SignupDto): Promise<ICredentialsType> {
    const user = await this.userService.createUser(context);
    return { _id: user._id, username: user.username, email: user.email };
  }
}
