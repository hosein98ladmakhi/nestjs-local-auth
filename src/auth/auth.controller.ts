import { Body, Controller, Post, Req, UseGuards } from '@nestjs/common';
import { AuthGuardLocal } from './auth.guard';
import { AuthService } from './auth.service';
import { SignupDto } from './dtos/signup.dto';
import { ICredentialsType } from './types/credentials.type';

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @UseGuards(AuthGuardLocal)
  @Post('login')
  async loginUser(
    @Req() { user }: { user: string },
  ): Promise<{ message: string }> {
    return { message: `Logged in with id : ${user}` };
  }

  @Post('signup')
  async signupUser(@Body() context: SignupDto): Promise<ICredentialsType> {
    return await this.authService.signup(context);
  }
}
