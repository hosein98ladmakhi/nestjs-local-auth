export interface ICredentialsType {
  _id: string;
  username: string;
  email: string;
}
