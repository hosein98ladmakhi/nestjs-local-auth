import { BadRequestException, Injectable } from '@nestjs/common';
import { PassportStrategy } from '@nestjs/passport';
import { Strategy } from 'passport-local';
import { AuthService } from './auth.service';

@Injectable()
export class LocalStrategy extends PassportStrategy(Strategy) {
  constructor(private authService: AuthService) {
    super({
      usernameField: 'email',
    });
  }

  async validate(userEmail: string, userPassword: string) {
    const user = await this.authService.login({
      email: userEmail,
      password: userPassword,
    });
    if (!user) throw new BadRequestException('Login Process Failed ...');
    return user._id;
  }
}
