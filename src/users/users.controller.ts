import { Controller, Get, UseGuards, Req } from '@nestjs/common';
import { AuthGuardLocal } from 'src/auth/auth.guard';
import { UsersService } from './users.service';
import { UserDocument } from './users.schema';

@Controller('users')
export class UsersController {
  constructor(private readonly userService: UsersService) {}

  @UseGuards(AuthGuardLocal)
  @Get('profile')
  async getProfile(@Req() req: { user: string }): Promise<UserDocument> {
    return await this.userService.findById(req.user);
  }
}
