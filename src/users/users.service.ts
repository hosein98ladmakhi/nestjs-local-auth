import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { User, UserDocument } from './users.schema';
import { Model } from 'mongoose';
import { CreateUserDto } from './dtos/create-user.dto';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel(User.name) private readonly userModel: Model<UserDocument>,
  ) {}

  async createUser(context: CreateUserDto): Promise<UserDocument> {
    const { username, email, password } = context;
    const passwordHash = await bcrypt.hash(password, 8);
    const user = new this.userModel({
      username,
      email,
      password: passwordHash,
    });
    return await user.save();
  }

  async findByEmail(email: string): Promise<UserDocument> {
    return await this.userModel.findOne({ email });
  }

  async findById(id: string): Promise<UserDocument> {
    return await this.userModel.findById(id).select('-password');
  }
}
